<?php
require_once ("../../../vendor/autoload.php");

$objSummery = new \App\SummeryOfOrg\Summery();
$objSummery->setData($_POST);
$objSummery->update();

\App\Utility\Utility::redirect("index.php");