<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\SummeryOfOrg\Summery();
$obj->setData($_GET);

$singleData = $obj->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/summery.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Summery Of Organization Update</title>
</head>
<body>

<div class="container">
    <div id="content">
        <h1>User Comment</h1>
        <form action="update.php" method="post">
            <div class= "wrapper">
                <input type="text" name="name" value="<?php echo $singleData->name?>" >
            </div>
            <div class="text">
                <textarea cols="40" rows="3" name="summery"><?php echo $singleData->summery?></textarea>
            </div>
            <div>
                <input type="submit" value="Update">
            </div>
            <div>
                <input type="hidden" name="id" value="<?php echo $singleData->id?>">
            </div>
        </form>
    </div>
</div>

</body>
</html>