<?php
require_once ("../../../vendor/autoload.php");

$path = $_SERVER ['HTTP_REFERER'];

$obj = new \App\City\City();

$obj->setData($_GET);
$obj->delete();

\App\Utility\Utility::redirect($path);