<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\City\City();
$obj->setData($_GET);

$obj->recover();

\App\Utility\Utility::redirect('index.php');