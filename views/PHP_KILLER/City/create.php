<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/city.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>City Select</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <h1>INPUT CITY NAME</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder="Enter Your Name..." required>
            </div>

            <div class="country">
               <select name="country" required>
                   <option value="" disabled selected>Please Select country...</option>
                   <option name="country" value="Arab">Arab</option>
                   <option name="country" value="America">America</option>
                   <option name="country" value="Australia">Australia</option>
                   <option name="country" value="Argentina">Argentina</option>
                   <option name="country" value="Bangladesh">Bangladesh</option>
                   <option name="country" value="Brazil">Brazil</option>
                   <option name="country" value="England">England</option>
                   <option name="country" value="India">India</option>
                   <option name="country" value="Pakistan">Pakistan</option>
                   <option name="country" value="South Africa">South Africa</option>
                   <option name="country" value="Span">Span</option>
                   <option name="country" value="New Zeland">New Zeland</option>
                   <option name="country" value="West Indies">West Indies</option>
                   <option name="country" value="Canada">Canada</option>
               </select>
           </div>
            <div>
                <input type="submit" value="Submit">
            </div>
            <div class="msg">
                <?php
                require_once ("../../../vendor/autoload.php");

                $msg = \App\Message\Message::message();

                echo "<div id='message'>".$msg."</div>";
                ?>
            </div>
        </form>
    </div>
</div>

</body>
</html>