<?php
require_once ("../../../vendor/autoload.php");

$objCity = new \App\City\City();
$objCity->setData($_GET);

$singleData = $objCity->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/city.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>City Select</title>
</head>
<body>
<div class="container">
    <div class="content">
        <h1>INPUT CITY NAME</h1>
        <form action="update.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" value="<?php echo $singleData->name?>" >
            </div>

            <div class="country">
               <select name="country">
                   <option value="" disabled selected>Please Select country...</option>
                   <option name="country" value="Arab" <?php if ($singleData->city == 'Arab') echo "selected "?>>Arab</option>
                   <option name="country" value="America" <?php if ($singleData->city == "America") echo "selected"?>>America</option>
                   <option name="country" value="Australia" <?php if ($singleData->city == "Australia") echo "selected"?>>Australia</option>
                   <option name="country" value="Argentina" <?php if ($singleData->city == "Argentina") echo "selected"?>>Argentina</option>
                   <option name="country" value="Bangladesh" <?php if ($singleData->city == "Bangladesh") echo "selected"?>>Bangladesh</option>
                   <option name="country" value="Brazil" <?php if ($singleData->city == "Brazil") echo "selected"?>>Brazil</option>
                   <option name="country" value="England" <?php if ($singleData->city == "England") echo "selected"?>>England</option>
                   <option name="country" value="India" <?php if ($singleData->city == "India") echo "selected"?>>India</option>
                   <option name="country" value="Pakistan" <?php if ($singleData->city == "Pakistan" ) echo "selected"?>>Pakistan</option>
                   <option name="country" value="Pakistan" <?php if ($singleData->city == "Pakistan") echo "selected"?>>South Africa</option>
                   <option name="country" value="Span" <?php if ($singleData->city == "Span") echo "selected"?>>Span</option>
                   <option name="country" value="New Zeland" <?php if ($singleData->city == "New Zeland") echo "selected"?>>New Zeland</option>
                   <option name="country" value="West Indies" <?php if ($singleData->city == "West Indies" ) echo "selected"?>>West Indies</option>
                   <option name="country" value="Canada" <?php if ($singleData->city == "Canada") echo "selected"?>>Canada</option>
               </select>
           </div>
            <div>
                <input type="submit" value="Submit">
            </div>
            <div>
                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
            </div>
        </form>
    </div>
</div>

</body>
</html>