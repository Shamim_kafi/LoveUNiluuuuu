<?php
require_once ("../../../vendor/autoload.php");

$objProfile = new \App\profilePicture\ProfilePicture();


$fileName = $_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];
$destination = "images/".$fileName;
move_uploaded_file($source,$destination);

$_POST['profilePic'] = $fileName;

$objProfile->setData($_POST);
$objProfile->update();

\App\Utility\Utility::redirect("index.php");