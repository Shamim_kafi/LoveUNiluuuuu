<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\profilePicture\ProfilePicture();

$obj->setData($_GET);

$obj->trash();

\App\Utility\Utility::redirect('trashed.php');