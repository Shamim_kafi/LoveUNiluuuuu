<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\profilePicture\ProfilePicture();

$obj->setData($_GET);
$singleData = $obj->view();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/profile_pic.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Picture Edit</title>
</head>
<body>
<div class="container">
    <div id="content">
        <h1>User Comment</h1>
        <form action="update.php" method="post" enctype="multipart/form-data">
            <div class= "wrapper">
                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
                <input type="text" name="name" value="<?php echo $singleData->name?>">
            </div>
            <div class="file">
                <input type="file" name="image" accept=".png, .jpg">
                <img src="images/<?php echo $singleData->profile_picture ?>" alt=""  height="80" width="150">
            </div>
            <div>
                <input type="submit" value="Update">
            </div>
        </form>
    </div>
</div>

</body>
</html>