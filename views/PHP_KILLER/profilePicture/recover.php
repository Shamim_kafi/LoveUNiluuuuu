<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\profilePicture\ProfilePicture();
$obj->setData($_GET);

$obj->recover();

\App\Utility\Utility::redirect('index.php');