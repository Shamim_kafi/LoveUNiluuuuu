<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Gender\Gender();

$obj->setData($_GET);

$obj->trash();

\App\Utility\Utility::redirect('trashed.php');