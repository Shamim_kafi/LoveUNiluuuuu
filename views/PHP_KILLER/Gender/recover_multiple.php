<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Gender\Gender();

$IDs  = $_POST['multiple'];

foreach ($IDs as $id ){

    $_GET['id'] = $id;
    $obj->setData($_GET);

    $obj->recover();
}


\App\Utility\Utility::redirect('index.php');