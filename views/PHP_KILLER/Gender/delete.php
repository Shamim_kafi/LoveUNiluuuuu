<?php
require_once ("../../../vendor/autoload.php");

$path = $_SERVER ['HTTP_REFERER'];

$obj = new \App\Gender\Gender();

$obj->setData($_GET);
$obj->delete();

\App\Utility\Utility::redirect($path);