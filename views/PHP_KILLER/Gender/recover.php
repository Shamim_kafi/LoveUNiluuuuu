<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Gender\Gender();
$obj->setData($_GET);

$obj->recover();

\App\Utility\Utility::redirect('index.php');