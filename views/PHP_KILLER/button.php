<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../resources/style/button.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.min.css">
    <link rel="stylesheet" href="../../resources/style/new/reset.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Delius+Swash+Caps" />
    <title>Atomic project</title>

</head>
<body>
<div class="nav">
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <ul class="pull-left">
                <li>
                    <a href="view.php" style="color: white;text-decoration: none;">Home</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-lg-6">
            <div class="logo">
                <h1>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">P</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">H</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">P</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">K</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">I</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">L</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">L</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">E</span>
                    <span style="border-radius: 50%;font-family: Delius Swash Caps">R</span>
                </h1>
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
            <ul class="pull-right">

            </ul>

        </div>
    </div>
</div>



<div class="container">
    <div class="atomic" align="center">
        <h2 style="color:blue;padding: 5px;font-size: 33px;font-family: Delius Swash Caps">ATOMIC PROJECT</h2>
    </div>
    <div class="first">
        <div class="row">
            <div class="col-md-4">
                <div class="flex high"> <a style="color: white" href="Book_Title/index.php" class="bttn-high">Book title</a></div>
            </div>
            <div class="col-md-4">
                <div class="flex dark"> <a style="color: white" href="Birthday/index.php" class="bttn-dark">Birthday</a></div>
            </div>
            <div class="col-md-4">
                <div class="flex"> <a style="color: white" href="Gender/index.php" class="bttn">Gender</a></div>
            </div>
        </div>
    </div>

    <div style="margin-bottom: 30px" class="second">
        <div class="row">
            <div class="col-md-3">
                <div class="flex soft"> <a style="color: white" href="profilePicture/index.php" class="bttn-soft">Profile picture</a></div>
            </div>
            <div class="col-md-3">
                <div class="flex"> <a style="color: white" href="Hobies/index.php" class="bttn">Hobbies</a></div>
            </div>
            <div class="col-md-3">
                <div class="flex light"> <a style="color: white" href="SummeryOfOrg/index.php" class="bttn-light">Summery of organization</a></div>
            </div>
            <div class="col-md-3">
                <div class="flex dark"> <a style="color: white" href="City/index.php" class="bttn-dark">City</a></div>
            </div>
        </div>
    </div>
</div>












<div class="container-fluid navbar-fixed-bottom" style="margin-bottom: 20px;">
    <div class="col-md-4"></div>

    <div class="col-md-4" align="center">
        <h4 style="color:ghostwhite;padding: 5px">Designed By- PHP KILLER</h4>
    </div>

    <div class="col-md-4"></div>

</div>
</body>
</html>