<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../resources/style/view.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.min.css">
    <link rel="stylesheet" href="../../resources/style/new/reset.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Delius+Swash+Caps" />
    <title>PHP KILLER</title>
</head>
<body>
<div class="nav">
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <ul class="pull-left">
                <li>
                    <a href="" style="color: white;text-decoration: none;">Home</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-lg-6">
            <div class="logo">
                <h1>
                    <span style="border-radius: 50%;font-style: Aladin">P</span>
                    <span style="border-radius: 50%;font-style: Aladin">H</span>
                    <span style="border-radius: 50%;font-style: Aladin">P</span>
                    <span style="border-radius: 50%;font-style: Aladin">K</span>
                    <span style="border-radius: 50%;font-style: Aladin">I</span>
                    <span style="border-radius: 50%;font-style: Aladin">L</span>
                    <span style="border-radius: 50%;font-style: Aladin">L</span>
                    <span style="border-radius: 50%;font-style: Aladin">E</span>
                    <span style="border-radius: 50%;font-style: Aladin">R</span>
                </h1>
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
              cl<ul class="pull-right">
                    <li>
                        <a href="button.php" style="color: white;text-decoration: none;">Atomic Project</a>
                    </li>
                </ul>

        </div>
    </div>
</div>

<div class="container-fluid">
    <h2 class="wow fadeInUp" data-wow-delay = "2.0s" style="color: moccasin;margin-top: 20px;padding: 10px;font-size: 30px">GROUP MEMBER</h2>
    <div class="container-fluid Group-member">

        <div class="col-md-4 col-sm-4 col-xs-4 member" align="center">
            <ul>
                <li class="wow fadeInRight" data-wow-delay="2.5s">
                    Shamim Kafi
                    <br>
                    SEID:172199
                </li>
                <li class="wow fadeInRight" data-wow-delay="3.8s">
                    Yusuf Bin Nur
                    <br>
                    SEID:172199
                </li>
                <li class="wow fadeInRight" data-wow-delay="5.2s">
                    Rihan Subhan
                    <br>
                    SEID:171250
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 member" align="center">
            <ul>
                <li class="wow fadeInLeft" data-wow-delay="3.3s">
                    Sharif Khan
                    <br>
                    SEID:172199
                </li>
                <li class="wow fadeInLeft" data-wow-delay="4.2s">
                    Shah Md. Rizve
                    <br>
                    SEID:172199
                </li>
                <li class="wow fadeInLeft" data-wow-delay="4.7s">
                    Tanzim Ahmed
                    <br>
                    SEID:172199
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="container-fluid">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h3 class="wow fadeInDown" data-wow-delay="5.6s" align="center">Presented By
            <br>
            Abu Zayed Hasnain Masum
        </h3>
    </div>

    <div class="col-md-3"></div>
</div>

<script src="../../resources/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>
</body>
</html>