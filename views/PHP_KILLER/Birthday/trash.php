<?php
require_once ("../../../vendor/autoload.php");

$objBirthday = new \App\Birthday\Birthday();

$objBirthday->setData($_GET);
$objBirthday->trash();

\App\Utility\Utility::redirect("trashed.php");