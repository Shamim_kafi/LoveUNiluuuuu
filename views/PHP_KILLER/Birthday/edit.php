<?php
require_once("../../../vendor/autoload.php");

$obj = new \App\Birthday\Birthday();

$obj->setData($_GET);
$singleData = $obj->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resources/style/birthday.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div id="content">
        <h1>BirthDay Date</h1>
        <form action="update.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" value="<?php echo $singleData->name ?>" placeholder="Enter Your Name">
            </div>
            <div class="wrapper">
                <input type="date" name="dob" value="<?php echo $singleData->dob ?>">
            </div>
            <div>
                <input type="submit" value="Update">
            </div>
            <div>
                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
            </div>
        </form>
    </div>
</div>
</body>
</html>