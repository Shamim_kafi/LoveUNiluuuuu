<?php
require_once ("../../../vendor/autoload.php");

$objBirthday = new \App\Birthday\Birthday();

$objBirthday->setData($_GET);
$objBirthday->recover();

\App\Utility\Utility::redirect("index.php");