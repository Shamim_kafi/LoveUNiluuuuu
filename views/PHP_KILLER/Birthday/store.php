<?php
require_once("../../../vendor/autoload.php");

$objBirthdayClass = new \App\Birthday\Birthday();

$objBirthdayClass->setData($_POST);

$objBirthdayClass->store();

\App\Utility\Utility::redirect('create.php');