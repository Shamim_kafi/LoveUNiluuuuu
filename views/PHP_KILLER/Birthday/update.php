<?php
require_once("../../../vendor/autoload.php");

$objBirthdayClass = new \App\Birthday\Birthday();

$objBirthdayClass->setData($_POST);

$objBirthdayClass->update();

\App\Utility\Utility::redirect('index.php');