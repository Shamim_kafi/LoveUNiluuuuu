<?php
require_once ("../../../vendor/autoload.php");
$obj = new \App\Birthday\Birthday();
$obj->setData($_GET);
$singleData = $obj->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <table class="table-bordered table table-striped">
                    <h2 style="text-align: center">Single Record Information - Birthday</h2>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Date Of Birth</th>
                    </tr>
                    <?php
                        echo "
                        
                        <tr>                       
                            <td>$singleData->id</td>                                   
                            <td>$singleData->name</td>                                   
                            <td>$singleData->dob</td>                                   
                        </tr>
                                     
                        ";
                    ?>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

</body>
</html>
