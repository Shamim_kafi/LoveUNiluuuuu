<?php
require_once ("../../../vendor/autoload.php");

$path = $_SERVER ['HTTP_REFERER'];

$obj = new \App\BookTitle\BookTitle();

$IDs = $_POST['multiple'];

foreach ($IDs as $id){

    $_GET['id'] = $id;
    $obj->setData($_GET);
    $obj->delete();
}

\App\Utility\Utility::redirect($path);