<?php
require_once ('../../../vendor/autoload.php');
use App\BookTitle\BookTitle;

$obj= new BookTitle();
$allData = $obj->index();


$trs="";
$sl=0;

    foreach($allData as $row) {
        $id =  $row->id;
        $bookTitle = $row->book_title;
        $authorName =$row->author_name;
        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='250'> $bookTitle </td>";
        $trs .= "<td width='250'> $authorName</td>";

        $trs .= "</tr>";
    }

$html= <<<BITM

<head>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
</head>


<div class="table-responsive">

            <h2 align = 'center'>PHP KILLER</h2>
            <table style = 'text-align: center' class="table table-bordered " >
                <thead>
                <tr>
                    <th align='left' style='color:red'>Serial</th>
                    <th align='left' style='color:red'>ID</th>
                    <th align='left' style='color:red'>Book Title</th>
                    <th align='left' style='color:red'>Author Name</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Book.pdf', 'D');


