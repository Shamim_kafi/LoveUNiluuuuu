<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\BookTitle\BookTitle();
$obj->setData($_GET);

$obj->recover();

\App\Utility\Utility::redirect('index.php');