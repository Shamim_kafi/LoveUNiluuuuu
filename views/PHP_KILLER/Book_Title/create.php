
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resources/style/book_title.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div id="content">
        <a href="index.php"><button class="btn btn-info">Index</button></a>
        <h1>book title</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="bookTitle" placeholder="Book Name" required>
            </div>
            <div class="wrapper">
                <input type="text" name="authorName" placeholder="Author Name" required>
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>

            <?php

            require_once("../../../vendor/autoload.php");

            use App\Message\Message;

            $msg = Message::message();

            echo "<div>  
                 <div id='message' style='color: red;padding: 10px;font-size: 18px'>  $msg </div>
            </div>";

            ?>
        </form>
    </div>
</div>
<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

</body>
</html>