<?php
require_once("../../../vendor/autoload.php");

$obj = new \App\BookTitle\BookTitle();
$obj->setData($_GET);
$singleData = $obj->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resources/style/book_title.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div id="content">
        <h1>book title</h1>
        <form action="update.php" method="post">
            <div class="wrapper">
                <input type="text" name="bookTitle" value="<?php echo $singleData->book_title?>">
            </div>
            <div class="wrapper">
                <input type="text" name="authorName" placeholder="Author Name" value="<?php echo $singleData->author_name?>">
            </div>
            <div>
                <input type="submit" value="Update">
            </div>
            <div>
                <input type="hidden" name="id" value="<?php echo $singleData->id?>">
            </div>
        </form>
    </div>
</div>
</body>
</html>