<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/hobies.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Hobie Inserted</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body>
<div class="container">
    <a href="index.php"><button class="btn btn-danger">Index</button></a>
    <div class="content">
        <h1>Login Form</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder="Enter your name...">
            </div>
            <div class="hobie">
                <span>Hobies :</span>
                <input type="checkbox" name="hobie[]" value="Cooking">Cooking
                <input type="checkbox" name="hobie[]" value="Coloring">Coloring
                <input type="checkbox" name="hobie[]" value="Drawing">Drawing
                <input type="checkbox" name="hobie[]" value="Fashion">Fashion
                <input type="checkbox" name="hobie[]" value="Magic">Magic
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>
            <div class="msg">
                <?php
                require_once ("../../../vendor/autoload.php");

                $msg = \App\Message\Message::message();

                echo "<div id='message'>".$msg."</div>";
                ?>
            </div>
        </form>
    </div>
</div>
</body>
</html>