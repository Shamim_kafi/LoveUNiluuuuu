<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Hobies\Hobies();
$obj->setData($_GET);

$obj->recover();

\App\Utility\Utility::redirect('index.php');