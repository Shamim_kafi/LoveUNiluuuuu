<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Hobies\Hobies();
$obj->setData($_GET);
$singleData = $obj->view();

$array = explode(",",$singleData->hobies);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/hobies.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Hobie updated</title>
</head>
<body>
<div class="container">
    <div class="content">
        <h1>Login Form</h1>
        <form action="update.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" value="<?php echo $singleData->name ?>">
            </div>
            <div class="hobie">
                <span>Hobies :</span>
                <input type="checkbox" name="hobie[]" value="Cooking" <?php if (in_array("Cooking",$array)) echo "checked"?>>Cooking
                <input type="checkbox" name="hobie[]" value="Coloring"<?php if (in_array("Coloring",$array)) echo "checked"?>>Coloring
                <input type="checkbox" name="hobie[]" value="Drawing"<?php if (in_array("Drawing",$array)) echo "checked"?>>Drawing
                <input type="checkbox" name="hobie[]" value="Fashion" <?php if (in_array("Fashion",$array)) echo "checked"?>>Fashion
                <input type="checkbox" name="hobie[]" value="Magic" <?php if (in_array("Magic",$array)) echo "checked"?>>Magic
            </div>
            <div>
                <input type="submit" value="Update">
            </div>
            <div>
                <input type="hidden" name="id"  value="<?php echo $singleData->id ?>">
            </div>
        </form>
    </div>
</div>
</body>
</html>