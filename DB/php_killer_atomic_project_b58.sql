-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2017 at 12:17 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_killer_atomic_project_b58`
--
CREATE DATABASE IF NOT EXISTS `php_killer_atomic_project_b58` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `php_killer_atomic_project_b58`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `dob`, `is_trashed`) VALUES
(1, 'Shamim kafi', '1996-06-30', 'NO'),
(2, 'Rista', '1999-07-05', 'NO'),
(3, 'Dola', '1996-04-03', 'NO'),
(4, 'Jarin', '1999-06-01', 'NO'),
(5, 'Bito', '1988-07-28', 'NO'),
(6, 'Mishu', '2017-06-06', 'NO'),
(7, 'Jishan', '2017-06-05', 'NO'),
(8, 'Rian', '2017-06-04', 'NO'),
(9, 'Rista', '2017-06-14', 'NO'),
(10, 'kafi', '2017-06-30', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(3, 'Toy', 'Alex Devies', 'NO'),
(4, 'Himu', 'Humayon Ahmed', 'NO'),
(6, 'Mon', 'Monisha', 'NO'),
(7, 'Bangladeshi Man', 'Kazi Nazrul Islam', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_trashed`) VALUES
(1, 'Shamim', 'Bangladesh', 'NO'),
(2, 'Sami', 'Bangladesh', 'NO'),
(3, 'Munna', 'Australia', 'NO'),
(4, 'Rakin', 'Arab', '2017-06-11 17:30:03'),
(5, 'Farhad', 'England', '2017-06-11 17:30:03'),
(6, 'Rian', 'Span', '2017-06-11 17:30:03'),
(7, 'Munna', 'Australia', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `user_name`, `gender`, `is_trashed`) VALUES
(1, 'Zidan', 'Male', 'NO'),
(2, 'Shamim', 'male', 'NO'),
(3, 'Tahsina', 'Female', 'NO'),
(4, 'Touhida', 'Female', 'NO'),
(5, 'kafu', 'Male', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hobies`
--

CREATE TABLE `hobies` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobies` varchar(100) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobies`
--

INSERT INTO `hobies` (`id`, `name`, `hobies`, `is_trashed`) VALUES
(1, 'Shmaim', 'Cooking,Coloring', 'NO'),
(2, 'Dola', 'Cooking,Fashion,Drawing', 'NO'),
(3, 'kafi', 'Cooking,Drawing,Magic', 'NO'),
(4, 'Eco', 'Cooking,Drawing', 'NO'),
(5, 'Nusi', 'Cooking,Coloring,Fashion', 'NO'),
(6, 'Rista', 'Cooking,Coloring', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(2, 'Raihan', '', '2017-06-12 13:50:01'),
(3, 'Nila', '149605915318671191_640616002812637_8305875217886784212_n.jpg', 'NO'),
(5, 'Remon', '1496059355Latest-Sarkari-Results-For-Various-Exams.png', 'NO'),
(8, 'Yusuf', '1497128488new.jpg', 'NO'),
(9, 'Shorif', '1497128500SUSHI_japanese_food_rice_japan_asian_oriental_1sushi_fish_seafood_5184x3456.jpg', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `summery`
--

CREATE TABLE `summery` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `summery` varchar(255) NOT NULL,
  `is_trashed` varchar(20) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summery`
--

INSERT INTO `summery` (`id`, `name`, `summery`, `is_trashed`) VALUES
(1, 'Shmaim', 'How are You??', 'NO'),
(2, 'Shamim', 'jjj', 'NO'),
(3, 'fff', 'fff', 'NO'),
(4, 'Nila', 'ki khbr???', 'NO'),
(5, 'Shamim', 'ki obostrha', 'NO'),
(6, 'Shamim', 'kiiii', 'NO'),
(7, 'Rakin', 'I think this is the right think... best of luck...\r\n\r\n\r\nthank you so much......', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobies`
--
ALTER TABLE `hobies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summery`
--
ALTER TABLE `summery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobies`
--
ALTER TABLE `hobies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `summery`
--
ALTER TABLE `summery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
