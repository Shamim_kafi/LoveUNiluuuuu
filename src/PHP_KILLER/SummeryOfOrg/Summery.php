<?php
namespace App\SummeryOfOrg;

use PDO;
use App\Message\Message;
use App\Model\Database;

class Summery extends Database
{

    public $id;
    public $name;
    public $summery;

    public function setData($rcv){

        if (array_key_exists('id',$rcv)){
            $this->id =  $rcv ['id'];
        }
        if (array_key_exists('name',$rcv)){
            $this->name =  $rcv ['name'];
        }

        if (array_key_exists('summery',$rcv)){
            $this->summery =  $rcv ['summery'];
        }
    }//end set data

    public function store(){

        $name = $this->name;
        $summery =  $this->summery;
        $dataArray = array($name,$summery);

        $sql = "INSERT INTO `summery` (`name`, `summery`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully! Data has been inserted.. ");
        }else{

            Message::message("Data has not been inserted...");
        }
    }

    public function index(){

        $sqlQuery = "select * from summery WHERE is_trashed = 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;
    }
    public function view(){

        $sqlQuery = "select * from summery WHERE id= $this->id";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();
        return $singleData;
    }


    public function update(){

        $name = $this->name;
        $summery = $this->summery;

        $dataArray = array($name,$summery);
        $sqlQuery = "UPDATE `summery` SET `name` = ?, `summery` = ? WHERE `id` = $this->id;";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);
        if($result){
            Message::message("Successfully!! Data update...");
        }else {

            Message::message("Error!!!");
        }
    }
    public function trash(){

        $sqlQuery = "UPDATE `summery` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }
    }

    public function trashed(){

        $sqlQuery = "select * from summery WHERE is_trashed <> 'No'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `summery` SET is_trashed = 'No' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recover");
        }else{

            Message::message("Error!! ");
        }
    }

    public function delete(){

        $sqlQuery = "delete from summery WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byUser']) && isset($requestArray['bySum']) )  $sql = "SELECT * FROM `summery` WHERE `is_trashed` ='NO' AND (`name` LIKE '%".$requestArray['search']."%' OR `summery` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byUser']) && !isset($requestArray['bySum']) ) $sql = "SELECT * FROM `summery` WHERE `is_trashed` ='NO' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byUser']) && isset($requestArray['bySum']) )  $sql = "SELECT * FROM `summery` WHERE `is_trashed` ='NO' AND `summery` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->summery);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->summery);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from summery  WHERE is_trashed = 'NO' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }

}//end Summery class