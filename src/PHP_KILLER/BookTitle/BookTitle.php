<?php
namespace App\BookTitle;



use PDO;
use App\Model\Database;
use App\Message\Message;

class BookTitle extends Database
{

    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($postArray){

        if (array_key_exists('id',$postArray)){

            $this->id = $postArray['id'];
        }

        if (array_key_exists('bookTitle',$postArray)){

            $this->bookTitle = $postArray['bookTitle'];
        }
        if (array_key_exists('authorName',$postArray)){

            $this->authorName = $postArray['authorName'];
        }
    }//set Data

    public function store(){

        $bookName = $this->bookTitle;
        $authorName = $this->authorName;

        $dataArray = array($bookName,$authorName);

        $sqlQuery ="INSERT INTO `book_title` (`book_title`, `author_name`) VALUES (?,?);";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }
    }

    public function index(){

        $sqlQuery = "select * from book_title WHERE is_trashed = 'NO'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from book_title WHERE id=".$this->id;
        $sth = $this->db->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData =  $sth->fetch();

        return $singleData;
    }

    public function update(){

        $bookName = $this->bookTitle;
        $authorName = $this->authorName;

        $dataArray = array($bookName,$authorName);

        $sqlQuery ="UPDATE `book_title` SET `book_title` = ? , `author_name` = ? WHERE `id`=$this->id";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }
    }
    public function trash(){

        $sqlQuery = "UPDATE `book_title` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }

    }

    public function trashed(){

        $sqlQuery = "select * from book_title WHERE is_trashed <> 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `book_title` SET is_trashed = 'NO' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recovered");
        }else{

            Message::message("Error!! ");
        }

    }

    public function delete(){

        $sqlQuery = "DELETE from book_title  WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NO' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NO' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NO' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from book_title  WHERE is_trashed = 'NO' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }


}//End BookTitle Class
