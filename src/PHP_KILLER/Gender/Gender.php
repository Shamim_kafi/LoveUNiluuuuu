<?php
namespace App\Gender;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class Gender extends Database
{
    public $id;
    public $name;
    public $gender;

    public function setData($rcv){

        if (array_key_exists('id',$rcv)){
            $this->id = $rcv['id'];
        }
        if (array_key_exists('name',$rcv)){
            $this->name = $rcv['name'];
        }
        if (array_key_exists('Gender',$rcv)){
            $this->gender = $rcv['Gender'];
        }
    }//end of setData

    public function store(){

        $name = $this->name;
        $gender = $this->gender;
        $dataArray = array($name,$gender);

        $sql = "INSERT INTO `gender` (`user_name`, `gender`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully! Inserted Data...");
        }else {

            Message::message("Error!! Check Please..");
        }
    }

    public function index(){

        $sqlQuery = "select * from gender";
        $sth = $this->db->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from gender WHERE id =$this->id";
        $sth = $this->db->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update(){

        $name = $this->name;
        $gender = $this->gender;

        $dataArray = array($name,$gender);
        $sqlQuery = "UPDATE `gender` SET `user_name` = ?, `gender` = ? WHERE `id` = $this->id;";

        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Updated");
        }else{

            Message::message("Error!!!!!");
        }
    }
    public function trash(){

        $sqlQuery = "UPDATE `gender` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }

    }

    public function trashed(){

        $sqlQuery = "select * from gender WHERE is_trashed <> 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `gender` SET is_trashed = 'NO' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recovered");
        }else{

            Message::message("Error!! ");
        }

    }

    public function delete(){

        $sqlQuery = "DELETE from gender  WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byUser']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='NO' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `gender` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byUser']) && !isset($requestArray['byGender']) ) $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='NO' AND `user_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byUser']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='NO' AND `gender` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->gender);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->gender);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from gender  WHERE is_trashed = 'NO' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }






}//end of gender class