<?php
namespace App\profilePicture;

use PDO;
use App\Message\Message;
use App\Model\Database;

class ProfilePicture extends Database
{

    public $id;
    public $user_name;
    public $profilePic;

    public function setData($rcv){

        if (array_key_exists('id',$rcv)){

            $this->id =  $rcv['id'];
        }

        if (array_key_exists('name',$rcv)){

            $this->user_name =  $rcv['name'];
        }
        if (array_key_exists('profilePic',$rcv)){

            $this->profilePic =  $rcv['profilePic'];
        }
    }

    public function store(){

        $name= $this->user_name;
        $profileImg = $this->profilePic;

        $dataArray = array($name,$profileImg);

        $sql = "INSERT INTO `profile_picture` (`name`, `profile_picture`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);
        if ($result){

            Message::message("Successfully! Picture Inserted");
        }else {
            Message::message("Picture does not inserted...");
        }
    }

    public function index(){

        $sqlQuery = "select * from profile_picture WHERE is_trashed = 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from profile_picture WHERE id = $this->id";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update(){

        $name = $this->user_name;
        $profilePic = $this->profilePic;

        $array = array($name,$profilePic);

        $sqlQuery = "UPDATE `profile_picture` SET `name` = ?, `profile_picture` = ? WHERE `id` = $this->id;";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($array);

        if ($result){

            Message::message("Successfully!! Data Updated...");
        }else{

            Message::message("Error!! Please check your input...");
        }
    }

    public function trash(){

        $sqlQuery = "UPDATE `profile_picture` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }

    }

    public function trashed(){

        $sqlQuery = "select * from profile_picture WHERE is_trashed <> 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `profile_picture` SET is_trashed = 'NO' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recovered");
        }else{

            Message::message("Error!! ");
        }

    }

    public function delete(){

        $sqlQuery = "DELETE from profile_picture  WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byUser']) && isset($requestArray['byPicture']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `profile_picture` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byUser']) && !isset($requestArray['byPicture']) ) $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byUser']) && isset($requestArray['byPicture']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND `profile_picture` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->profile_picture);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->profile_picture);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from profile_picture  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }

}